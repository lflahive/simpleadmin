package com.lukeflahive;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class SimpleAdmin extends JavaPlugin{
	
	@Override
	public void onEnable(){
		getCommand("kick").setExecutor(new SimpleAdminCommandExecutor(this));
		getCommand("smite").setExecutor(new SimpleAdminCommandExecutor(this));
		getCommand("sethealth").setExecutor(new SimpleAdminCommandExecutor(this));
		getCommand("bring").setExecutor(new SimpleAdminCommandExecutor(this));
		getCommand("teleport").setExecutor(new SimpleAdminCommandExecutor(this));
		getCommand("ban").setExecutor(new SimpleAdminCommandExecutor(this));
		
		BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		scheduler.scheduleSyncRepeatingTask(this, new Runnable(){
			public void run(){
					Bukkit.broadcastMessage(ChatColor.GREEN + "Server is now checking the ban file.");
					try {
						File file = new File("SimpleAdminBans.txt");
						File temp = new File("TempSimpleAdminBans.txt");
						BufferedReader reader = new BufferedReader(new FileReader(file));
						BufferedWriter writer = new BufferedWriter(new FileWriter(temp));
						String line;
						
						long unixTime = System.currentTimeMillis() / 1000L;
						
						//Go through the file to check for bans
						while((line = reader.readLine()) != null){
							String[] lineArray = line.split(",");
							if(Long.parseLong(lineArray[1]) < unixTime){
								Bukkit.getOfflinePlayer(lineArray[0]).setBanned(false);
								Bukkit.broadcastMessage(ChatColor.DARK_PURPLE + "The player " + lineArray[0] + " has been unbanned.");
							} else {
								writer.write(line);
							}
						}
						writer.close();
						reader.close();
						file.delete();
						temp.renameTo(file);
						temp.delete();
						
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
		}, 0L, 1200L);
	}
	
	@Override
	public void onDisable(){
		
	}
	
}
