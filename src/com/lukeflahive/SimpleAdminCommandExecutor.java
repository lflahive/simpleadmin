package com.lukeflahive;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SimpleAdminCommandExecutor implements CommandExecutor{
	
	private SimpleAdmin simpleAdmin;
	
	public SimpleAdminCommandExecutor(SimpleAdmin simpleAdmin){
		this.simpleAdmin = simpleAdmin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		//Kick
		
		if(cmd.getName().equalsIgnoreCase("kick")){
			if(args.length == 1){
				if(!(sender instanceof Player)){
					sender.sendMessage("This command can only be used by players.");
				} else {
					Player target = sender.getServer().getPlayer(args[0]);
					target.kickPlayer("You have been kicked by an admin.");
				}
				return true;
			}
			return false;
		}
		
		//Smite
		
		if(cmd.getName().equalsIgnoreCase("smite")){
			if(args.length == 1){
				if(!(sender instanceof Player)){
					sender.sendMessage("This command can only be used by players.");
				} else {
					Player target = sender.getServer().getPlayer(args[0]);
					Player player = sender.getServer().getPlayer(sender.getName());
					target.getWorld().strikeLightning(target.getLocation());
					Bukkit.broadcastMessage(player.getName() + " smote " + target.getName());
					target.setHealth(0);
				}
				return true;
			}
			return false;
		}
		
		//SetHealth
		
		if(cmd.getName().equalsIgnoreCase("sethealth")){
			if(args.length == 2){
				if(!(sender instanceof Player)){
					sender.sendMessage("This command can only be used by players.");
				} else {
					Player target = sender.getServer().getPlayer(args[0]);
					Player player = sender.getServer().getPlayer(sender.getName());
					target.setHealth(Double.parseDouble(args[1]));
					Bukkit.broadcastMessage(player.getName() + " set " + target.getName() + " health to " + args[1]);
				}
				return true;
			}
			return false;
		}
		
		//Bring
		
		if(cmd.getName().equalsIgnoreCase("bring")){
			if(args.length == 1){
				if(!(sender instanceof Player)){
					sender.sendMessage("This command can only be used by players.");
				} else {
					Player target = sender.getServer().getPlayer(args[0]);
					Player player = sender.getServer().getPlayer(sender.getName());
					target.teleport(player.getLocation());
					Bukkit.broadcastMessage(player.getName() + " brought " + target.getName() + " to them.");
				}
				return true;
			}
			return false;
		}
		
		//Teleport
		
		if(cmd.getName().equalsIgnoreCase("teleport")){
			if(args.length == 2){
				if(!(sender instanceof Player)){
					sender.sendMessage("This command can only be used by players.");
				} else {
					Player target = sender.getServer().getPlayer(args[0]);
					Player target2 = sender.getServer().getPlayer(args[1]);
					Player player = sender.getServer().getPlayer(sender.getName());
					target.teleport(target2.getLocation());
					Bukkit.broadcastMessage(player.getName() + " teleported " + target.getName() + " to " + target2.getName());
				}
				return true;
			}
			return false;
		}
		
		/* Ban
		 * This will eventually have a time limit on once I add a MySQL connection. */
		
		if(cmd.getName().equalsIgnoreCase("ban")){
			if(args.length == 2){
				if(!(sender instanceof Player)){
					sender.sendMessage("This command can only be used by players.");
				} else {
					long unixTime = System.currentTimeMillis() / 1000L;
					unixTime = unixTime + Integer.parseInt(args[1]);
					Player target = sender.getServer().getPlayer(args[0]);
					Player player = sender.getServer().getPlayer(sender.getName());
					target.setBanned(true);
					target.kickPlayer("You have been banned from the server for " + args[1] + " seconds.");
					Bukkit.broadcastMessage(player.getName() + " banned player " + target.getName());
					
					try {
						File file = new File("SimpleAdminBans.txt");
						BufferedWriter output = new BufferedWriter(new FileWriter(file));
						output.write(target.getName() + "," + unixTime);
						output.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				}
				return true;
			}
			return false;
		}
		
		return false;
	}
	
}
